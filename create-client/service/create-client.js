const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

const createClient = async (item) => {
    await dynamo.putItem({ TableName: TABLE_NAME, Item: item });
}

module.exports = { createClient };