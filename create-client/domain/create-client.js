const aws = require('aws-sdk');
const sns = new aws.SNS();
const ddb = require('aws-sdk/clients/dynamodb');
const uuid = require('uuid');
const { enabledClient } = require('../helper/enable-client');
const { createClient } = require('../service/create-client');
const { CreateClientInputSchema } = require('../schema/command/create-client');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

 module.exports = async (commandPayload, commandMeta)=>{
    commandPayload.id = uuid.v1();
    const client = new CreateClientInputSchema(commandPayload, commandMeta).get();
    const { birth } = commandPayload;
    if( enabledClient(birth) ){
      await createClient(client);
     } else {
         return {
             statusCode: 400,
             body: 'El cliente no cumple con las restricciones de edad'
         }
     }
    const response = {
        statusCode: 200,
        body: 'Cliente creado',
    };
    return response;
 }
