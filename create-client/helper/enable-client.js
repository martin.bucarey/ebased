const moment = require('moment');

const enabledClient = (birth) => {
    const today = moment();
    const age= today.diff(moment(birth),'years');
    return age >= 18 && age <= 56 ?  true : false;
}

module.exports = { enabledClient };